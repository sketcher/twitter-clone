/**
 * /-(^-^)-/
 * -t DOMsway
 * -d Multichaining functions for manipulating DOM elements
 * -a jakob andersson
 * -s https://gitlab.com/sketcher/domsway.js
 */

function DOMsway(...args) {
    this.id = new Number(Math.random().toFixed(3))
    this.createdAt = new Date().getTime()
    this.version = 1.04
    this.updated = '19-03-03'
    this.elements = []
    this.options = {
        'attrs_kebab_case': true,   // Auto-replaces from object name dataTag = ( to ) => data-tag
        'error_level': 0            // todo; more structured error handling
    }
    this.limiter = args && Number.isInteger(args[args.length - 1]) ? args[args.length - 1] : Infinity

    // for applying each method to all elements
    this.loop = function (callback) {
        this.elements.map((value, index, arr) => {
            callback(value, index, arr, this.elements)
        })
    }

    // ? existing DOM object
    this.isValidElement = function (obj) {
        return (typeof HTMLElement === 'object'
            ? obj instanceof HTMLElement : obj && typeof obj === 'object'
            && obj !== null && obj.nodeType === 1 && typeof obj.nodeName === 'string')
    }

    // ? valid querySelector string
    this.isValidSelector = function (selector) {
        if (typeof selector !== 'string') { return false }
        return ((function () { try { document.createElement('br').querySelector(selector) } catch (e) { return false } return true })())
    }

    // ? options object
    var isOptionsObject = function (input, compareTo) {
        var providedKeys = Object.keys(input)
            , wantedKeys = Object.keys(compareTo)
        return typeof providedKeys.find(key =>
            wantedKeys.includes(key)
        ) === 'undefined' ? false : true;
    }
    //
    var isValidSelector = this.isValidSelector


    // Merged into a single array
    args.map((a, i) => {
        if (this.isValidSelector(a)) {
            const elements = Array.from(document.querySelectorAll(a))
                .filter(e => this.isValidElement(e));
            elements === undefined || elements.length == 0 ?
                null : elements.map(e => {
                    if (this.elements.length < this.limiter) {
                        this.elements.push(e)
                    }
                })
        }
        else if (this.isValidElement(a)) {
            // ? limiter isset in params
            if (this.elements.length < this.limiter) {
                this.elements.push(a)
            }
        }
        // ? options object and last argument todo; combine limiter into options(?)
        else if ((args.length - 1) === i && isOptionsObject(a, this.options)) {
            Object.keys(a).map(el => {
                if (this.options.hasOwnProperty(el) && typeof this.options[el] === typeof a[el]) {
                    this.options[el] = a[el]
                }
            })
        }
        /**
         * todo;
         *  - if multiple @ make all checks required
         *  - if targetProp is aray || object make the check fit with switch case
         *  - etc
         */
        else if (typeof a === 'string' && a.includes('@')) {
            var isValidStructure = true
            var elem
            var lambdaSplit = function (val, ite) {
                if (ite === 0) {
                    isValidStructure = isValidSelector(val) ? isValidStructure : false;
                    if (isValidStructure) {
                        elem = document.querySelectorAll(val)
                    }
                    return;
                }
                var cArr = val.split('=')
                return Array.isArray(cArr) ? cArr : undefined;
            }
            var checkIfKeyExist = function (k, o) {
                var key = k.shift()
                var val = k.pop()
                var r = Array.from(elem)
                    .map(e => {
                        if (e && key in e) {
                            return e
                        }
                    })
                    .filter(e => e)
                    // Include weight for near match(?)
                    .filter(e => e[key] === val);
                return r
            }
            a.split('@')
                .map((x, i, y) => lambdaSplit(x, i))
                .filter(x => x)
                .map((x, i, ori) => checkIfKeyExist(x, ori))
                .filter(x => x.length > 0)
                .map(x => {
                    this.elements.push(x)
                })

        }
        else {
            // Invalid argument
            // todo; else console.error => invalid options supplied
        }
    })
    return this
}

/*////////////////////////////////////////
// Queried element[s] manipulation
////////////////////////////////////////*/

/**
 * Add new child to element[s]
 */
DOMsway.prototype.add = function (elementOrType, traverse = true) {
    var ele = this.isValidElement(elementOrType) ?
        elementOrType : this.isValidSelector(elementOrType) ?
            document.createElement(elementOrType) : false
    if (ele) {
        this.loop((a, i, c, elements) => {
            var newEle = ele.cloneNode()
            a.appendChild(newEle)
            if (traverse) {
                elements[i] = a.children[a.childElementCount > 0 ? (a.childElementCount - 1) : 'undefined']
            }
        })
    }
    return this
}

/**
 * Alter element[s] attributes
 * @param {String} type
 * @param {Object} objs[...]
 */
DOMsway.prototype.attr = function (type, ...objs) {
    type = type.toLocaleLowerCase() || false
    this.loop(a => {
        try {
            //
            // todo; regex or .includes to minify this
            //
            switch (type) {
                case String(type.match(/^(app|join)/gi)):
                    for (j = 0; j < objs.length; j++) {
                        for (x in objs[j]) {
                            if (a.hasAttribute(x)) {
                                var joined = a.getAttribute(x) + objs[j][x]
                                a.setAttribute(x, joined)
                            }
                        }
                    }
                    break;
                case String(type.match(/^(set|add)/gi)):
                    for (j = 0; j < objs.length; j++) {
                        for (x in objs[j]) {
                            // [OPTION] attrs_kebab_case
                            // todo; currently not working
                            if (this.options.attrs_kebab_case) {
                                //convert camelCase to kebab-case for HTML
                                const capitalLetter = x.search(/[A-Z]/)
                                var tx = x
                                if (capitalLetter > 0) {
                                    tx = x.replace(/[A-Z]/, `-${x.charAt(capitalLetter).toLowerCase()}`)
                                }
                                a.setAttribute(tx, objs[j][x])
                            }
                            else {

                                a.setAttribute(x, objs[j][x])
                            }
                        }
                    }
                    break;
                case String(type.match(/^(del|rem)/gi)):
                    for (j = 0; j < objs.length; j++) {
                        if (typeof objs[j] === 'string') {
                            a.removeAttribute(objs[j])
                        }
                        for (x in objs[j]) {
                            a.removeAttribute(x)
                        }
                    }
                    break;
                default:
                    //...
                    break;
            }
        }
        catch (e) {
            console.error(`Error => ${e}`)
        }
    })
    return this
}

/**
 * Namespaced to .attr()
 */
DOMsway.prototype.attrs = DOMsway.prototype.attr

/**
 * Set pointer to first child
 */
DOMsway.prototype.child = function () {
    return this.traverse(1)
}

/**
 * Clones element[s] at the same position
 * @param {Bool} copyChildren
 */
DOMsway.prototype.duplicate = function (replaceOriginals = false, copyChildren = true) {
    this.loop((a, i, arr) => {
        if (replaceOriginals) {
            arr[i] = a.parentElement.appendChild(a.cloneNode(copyChildren))
        }
        else {
            a.parentElement.appendChild(a.cloneNode(copyChildren))
        }
    })
    return this
}

/**
 * Make a new query and fuse them with existing elements
 */
DOMsway.prototype.fuse = function (...selectors) {
    this.elements = this.elements.concat(new DOMsway(...selectors).get([]))
    return this
}
/**
 * Move element[s] to different branch of DOM-tree
 */
DOMsway.prototype.move = function (elementOrType) {
    var ele = this.isValidElement(elementOrType) ?
        elementOrType : this.isValidSelector(elementOrType) ?
            document.querySelector(elementOrType) : false
    if (ele) {
        this.loop((a, i, c, elements) => {
            a.appendChild(ele)
        })
    }
    return this
}
/**
 * Set pointer to parent element
 */
DOMsway.prototype.parent = function () {
    return this.traverse(-1)
}

/**
 * Remove element[s]
 */
DOMsway.prototype.remove = function () {
    this.loop(a => {
        a.parentElement.removeChild(a)
    })
    return this
}

/**
 * Include all sibling elements in active pointer
 */
DOMsway.prototype.siblings = function () {
    this.loop((a, i, arr) => {
        var x = a
        while (x.nextSibling) {
            x = x.nextSibling;
            arr.push(x)
        }
    })
    return this
}

/**
* Limit queried elements
* @param {Integer} limit
* @return {Object} this
*/
DOMsway.prototype.swarm = function (limit) {
    this.elements = this.elements.splice(0, limit);
    return this
}

/**
* Swap affected elements
* @param {Integer} depth
* @param {Integer} affected
*/
DOMsway.prototype.traverse = function (lvl = 0, siblings = Infinity) {
    var vld = this.isValidElement
        , traversedElement = {
            _parent: function (ori, l) {
                var ele = ori
                for (var j = l; j < 0; j++) {
                    ele = ele.parentElement
                }
                return ele
            },
            _child: function (ori, l) {
                var temp = ori;
                for (var j = 0; j < l; j++) {
                    if (vld(temp)) {
                        temp = temp.children[0]
                    }
                }
                return temp
            },
        }
    try {
        if (!Number.isInteger(lvl)) {
            throw 'not integer'
        }
    }
    catch (e) {
        console.error(`ERROR => ${e}`)
        return this
    }
    this.loop((a, i, arr) => {
        if (lvl === -1) {
            arr[i] = a.parentElement
        }
        else if (lvl > 0) {
            arr[i] = traversedElement._child(a, lvl)
        }
        else if (lvl < 0) {
            arr[i] = traversedElement._parent(a, lvl)
        }
    })
    return this
}

/*////////////////////////////////////////
// Element affectations
////////////////////////////////////////*/

/**
 * Bind event onto element[s] 
 * @param {String} type
 * @param {Function} callbackFunction
 * @param {Boolean} boolType
 */
DOMsway.prototype.event = function (type, callbackFunction, boolType = false) {
    this.loop(a => {
        a.addEventListener(type, callbackFunction, boolType)
    })
    return this
}

/**
 * Combined use of .classList function
 * @param {String} type
 * @param {...[]} args
 */
DOMsway.prototype.class = function (type = 'add', ...args) {
    type = type.toLocaleLowerCase() || false
    this.loop(a => {
        try {
            // todo; classlist .item() && .contains for use elsewhere
            if (['add', 'remove',].includes(type)) {
                a.classList[type](...args)
            }
            else if (['toggle'].includes(type)) {
                a.classList[type](args[0])
            }
            else if (['replace'].includes(type)) {
                a.classList[type](args[0], args[1])
            }
        }
        catch (e) {
            console.error(`Error => ${e}`)
        }
    })
    return this
}

/**
* Write into the element's textContent || [or value for some cases] <--todo;
* @param {String} str
*/
DOMsway.prototype.write = function (str) {
    this.loop(a => {
        a.textContent = str
    })
    return this
}

/*////////////////////////////////////////
// Exprimental methods
////////////////////////////////////////*/

/**
 *  Change style 
 *  @param {...Object} objs
 */
DOMsway.prototype.style = function (...objs) {
    this.loop(a => {
        for (j = 0; j < objs.length; j++) {
            for (x in objs[j]) {
                var value = objs[j][x]
                    , key = x
                    , style = window.getComputedStyle(a);
                if (typeof style[x] === 'string') {
                    a.style[key] = value
                }
                else { console.error('no', x) }
            }
        }
    })
    return this
}

/**
 * 
 */
DOMsway.prototype.target = function () {
    var E = new Event('')
    return this
}

/**
 * 
 */
DOMsway.prototype.throttle = function () {
    return this
}

/*////////////////////////////////////////
// None DOM-object or void returning methods
////////////////////////////////////////*/

DOMsway.prototype.betaForm = function (schema = {}) {
    var form
        , inputs = []
        , input = {}
        , types  = ['button','checkbox','color','date','text','email','file','hidden','image','text','number','password','radio','range','reset','search','submit','tel','text','time','url','text']
        , attrs = ['data-regex', 'data-required', 'data-default']
        , testPattern = function(s, target){
            var {regex, onError, onSuccess} = s
            var {value} = target
            regex.test(value) ? onSuccess.call(null, target, value): onError.call(null, target, value);
        };
    this.loop((a, i, arr) => {
        if (a.tagName === 'FORM') { form = a }
    })
    
    inputs = [...form.querySelectorAll('input:not([disabled]):not([type="submit"])')]
    inputs
        // Set default value as previous value
        .map(inp=>{
            inp.setAttribute('data-prev', inp.value)
            return inp
        })
        // Makes this 'on-dropped focus' instead of onChange
        .map(inp=>inp.addEventListener('change',function(ev){
            var name = ev.target.id || ev.target.name || ev.target.type

            if(schema[name]){
                testPattern(schema[name], ev.target)  
            }
            else if(Object.keys(schema).includes(ev.target.type)){
                testPattern(schema[ev.target.type], ev.target)
            }

        }));

    form.addEventListener('submit', function(e){
        e.preventDefault()
        var json = {}
        inputs
            .map(inp=>{
                var name = inp.id || inp.name || inp.type;
                json[name] = inp.value
            })
        console.table(json)
    })
    
}


/**
 * When form is submitted
 * @param {Object} Options
 * @param {Function} Callback
 */
DOMsway.prototype.form = function (options = {}, callback = function (x) { alert(`Submitted ${x}`) }) {
    /*
    *   todo;
    *       - Add input field filters from options
    *       - options.preventDefault true || false
    */
    var type = options.type || 'JSON'
        , prvDef = typeof options.preventDefault === "boolean"
            ? options.preventDefault : true
        , form

    this.loop(a => {
        a.onsubmit = function (e) {
            if (prvDef) {
                e.preventDefault()
            }
            if (type.toUpperCase() === 'JSON') {
                form = (function (ev) {
                    var obj = {}
                    var eles = a.querySelectorAll('input:not([disabled]):not([type="submit"])')
                    for (var j = 0; j < eles.length; j++) {
                        if (eles[j].hasAttribute('name')) {
                            obj[eles[j].name] = eles[j].value
                        }
                        else {
                            obj[j] = eles[j].value
                        }
                    }
                    return JSON.stringify(obj)
                })()
            }
            else {
                //...
            }
            callback(form)
        }
    })
}

/**
 * Make an async request (url, method, options)
 * @param {String} url
 * @param {String} method
 * @param {Object} options
 */
DOMsway.prototype.request = function (...args) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest()
            , url = args[0]
            , method = args[1].toUpperCase() || 'GET'
            , options = args[2] || {
                header: {}// todo; set defaults
            }

        xhr.open(method, url)

        Object.keys(options.header)
        .map(key=>{
            xhr.setRequestHeader(key, options.header[key]);
        })
        
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhr.response)
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                })
            }
        }
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            })
        }
        if (args[2] && args[2]['body']) {
            xhr.send(args[2]['body'])
            return;
        }
        xhr.send()
    })
}


/**
* Add to the end for the debugging
* @returns {void} Void
*/
DOMsway.prototype.debug = function () {
    console.table(this.elements)
    return this
}

/**
 * Whetever element[s] exists
 * @returns {Boolean} Boolean
 */
DOMsway.prototype.exists = function () {
    var bool = true
    if (!Array.isArray(this.elements) || !this.elements.length) {
        return false
    }
    this.loop(a => {
        bool = this.isValidElement(a) ? bool : false
    })
    return bool
}

/**
 * Return array of elements
 * @param {Integer} Limiter
 * @return {Array} Elements
 */
DOMsway.prototype.get = function (n = 0) {
    return n && typeof n === "number" ? this.elements[n] : n && Array.isArray(n) ? this.elements : this.elements.length < 1 ? this.elements : this.elements[this.elements.length - 1]
}

/*
    Window binding
                    */
if (typeof window === 'object') {
    window.__proto__._ = function (...e) {
        return new DOMsway(...e)
    }
}
else if (module) {
    module.exports = DOMsway
}
else {
    console.error("%c No window or module detected for DOMsway to work",
        "font-size:150%; background-color:lightblue; font-family:helvetica; color: darkgreen;padding:0 25px;")
}
