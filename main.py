from flask import Flask, render_template, jsonify

app = Flask(__name__)
app.config.from_object('config')



@app.route("/")
def home():
    return render_template("home.html")


@app.route("/whine/<int:post_id>",methods=["POST"])
def whine(post_id):
    return jsonify(
        username="g.user.username",
        email="g.user.email",
        id="g.user.id"
    )


if __name__ == "__main__":
    app.run(debug=True)
